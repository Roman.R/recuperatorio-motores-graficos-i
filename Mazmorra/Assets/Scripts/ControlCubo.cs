using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCubo : MonoBehaviour
{ 
    bool tengoQueBajar = false;
    
    int rapidez = 4;
    
    void Update() 
    { 
        if (transform.position.y >= 7) 
        {  
            tengoQueBajar = true;
        
        } 
        
        if (transform.position.y <= 3) 
        { 
            tengoQueBajar = false;
        
        } 
        
        if (tengoQueBajar) 
        { 
            Bajar();
        } 
        else 
        { 
            Subir();
        } 
    
    } 
    
    void Subir() 
    { 
        transform.position += transform.up * rapidez * Time.deltaTime;
    } 
    
    void Bajar() 
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    
    } 
}

