using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        // Comprobar si el jugador toca la lava
        if (collision.gameObject.CompareTag("Lava"))
        {
            RestartGame();
        }
    }

    private void RestartGame()
    {
        // Reiniciar el juego cargando la escena actual
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.buildIndex);
    }
}

