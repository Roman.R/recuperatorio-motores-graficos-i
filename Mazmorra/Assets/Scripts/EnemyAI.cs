using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public Transform player;
    public float visionRange = 10f;
    public float speed = 5f;

    private void Update()
    {
        // Calcula la distancia entre el enemigo y el jugador
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);

        // Si el jugador est� dentro del rango de visi�n
        if (distanceToPlayer <= visionRange)
        {
            // Calcula la direcci�n hacia el jugador
            Vector3 directionToPlayer = (player.position - transform.position).normalized;

            // Mueve el enemigo hacia el jugador
            transform.Translate(directionToPlayer * speed * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Si el enemigo toca al jugador
        if (collision.gameObject.CompareTag("Player"))
        {
            // Reinicia el juego (carga la escena actual)
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        }
    }
}

